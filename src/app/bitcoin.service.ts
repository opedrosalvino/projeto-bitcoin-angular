import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
interface BitCoinRate {
  time: {
    updated: string;
  };
  bpi: {
    USD: {
      rate_float: number;
    };
    BRL: {
      rate_float: number;
    };
  };
}

@Injectable()
export class BitcoinService {
  bitCoinRates: Array<BitCoinRate> = [];

  constructor(private http: HttpClient) {
    if (this.bitCoinRates.length == 0) {
      this.updateBitCoinRates();
    }
    this.contadorAtualizar();
  }

  compararValores(valorAnt: number, valorNew: number) {
    if (valorAnt != valorNew) {
      return 1;
    } else {
      return 0;
    }
  }

  contadorAtualizar() {
    setInterval(() => {
      this.updateBitCoinRates();
    }, 60000);
  }

  updateBitCoinRates() {
    this.http
      .get<BitCoinRate>('https://api.coindesk.com/v1/bpi/currentprice/BRL.json')
      .subscribe((data) => {
        if (this.bitCoinRates.length == 0) {
          this.bitCoinRates.push(data);
        }

        if (
          this.compararValores(
            this.bitCoinRates[this.bitCoinRates.length - 1].bpi.BRL.rate_float,
            data.bpi.BRL.rate_float
          ) != 0 ||
          this.compararValores(
            this.bitCoinRates[this.bitCoinRates.length - 1].bpi.USD.rate_float,
            data.bpi.USD.rate_float
          ) != 0
        ) {
          this.bitCoinRates.push(data);
        }
      });
  }
}
